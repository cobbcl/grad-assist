<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Home| Grad Assist</title>

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet" type="text/css">

    <!-- Styles -->
    <style>
        html, body {
            background-color: #efefef;
            color: #636b6f;
            font-family: 'Nunito', sans-serif;
            font-weight: 200;
            height: 100vh;
            margin: 0;
            padding-bottom: 100px;
        }
        .full-height {
            height: 100vh;
        }
        .flex-center {
            align-items: center;
            display: flex;
            justify-content: center;
        }
        .position-ref {
            position: relative;
        }
        .top-right {
            position: absolute;
            right: 20px;
            top: 0px;
        }
        .top-left {
            position: absolute;
            left: 20px;
            top: 40px;
            color: rgb(254,254,254);
            font-size: 20px;
            font-weight: bold;
        }
        .content {
            padding: 5px;
            background: #efefef;
            text-align: center;
        }


        /*
        Top part
        */
        header, nav, footer {
            background-color: rgb(255,189,13);
            color: #eee;
            height: 10px;
            width:  100%;
            margin: 0;
            padding: 10px 10px;
            text-align: center;
        }
        nav {
            padding-bottom: 20px;
            background-color: #062766;
            height: 40px;
            overflow: hidden;
        }
        footer{

        }
        /*
        navigation bar
        */
        ul {
            list-style-type: none;
            margin: 0;
            padding: 0;
            overflow: hidden;
            background-color: #333;
        }

        li {
            float: left;
        }

        li a {
            display: block;
            color: white;
            text-align: center;
            padding: 14px 16px;
            text-decoration: none;
        }

        li a:hover {
            background-color: #062766;
        }
        /*
        navigation bar
        */
        .container {
            font-size: 84px;
            background-color: #fff;
            border-radius: 15px;
            box-shadow: 5px 5px 5px #888888;
            border: 1px solid #ffc82e;
            text-align: center;
            box-sizing: border-box;
            padding: 10px 100px;
        }

        .links > a {
            color: #636b6f;
            padding: 0 25px;
            font-size: 13px;
            font-weight: 600;
            letter-spacing: .1rem;
            text-decoration: none;
            text-transform: uppercase;
        }
        .loginButton{
            width: 300px;
            height: 40px;
            background-color: #ffffff;
            color: rgb(255,189,13);
            font-size: 20px;
            font-weight: bold;
        }

        .registerButton{
            width: 300px;
            height: 40px;
            background-color: #ffffff;
            color: rgb(255,189,13);
            font-size: 20px;
            font-weight: bold;
        }
        .m-b-md {
            border: 2px solid #ffc82e;
            background: #efefef;
        }
    </style>
</head>
<body background="/images/background.png">
<header>
    @if (Route::has('login'))
    @endif
    <div class="top-left">
        <div>GradAssist</div>
        <div>ETSU Grad Assistance</div>
    </div>


</header>
<nav></nav>
<div class="flex-center position-ref full-height">
    <div class="content">
        <div class="container">
            <div>
                <img src = "/images/etsulogo.png" height ="200" width="600">
            </div>
            <ul>
                    <li><a href="{{ route('login') }}" class="loginButton">Login</a></li>

                    @if (Route::has('register'))
                        <li><a href="{{ route('register') }}" class="registerButton">Register</a></li>
                    @endif
            </ul>
        </div>
    </div>
</div>

</body>
</html>