@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div>
                <img src = "/images/etsulogo2.png" height ="200" width="730">
            </div>
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">Modify a student</div>
                    <div class="card-body">
                        <form action="update" method="post">
                            @method('PUT')
                            @csrf()
                            <div class="form-group">
                                <label for="student_id">E Number</label>
                                <input type="text" class="form-control disabled" id="student_id" rows="1" placeholder="E00000000" name="student_id" value="{{$student->student_id}}">
                            </div>
                            <div class="form-group">
                                <label for="name">Name</label>
                                <input type="text" class="form-control" id="name" placeholder="Full name" name="name" value="{{$student->name}}">
                            </div>
                            <div class="form-group">
                                <label for="rating">Rating</label>
                                <input type="text" class="form-control" id="rating" rows="1" placeholder="5" name="rating" value="{{$student->rating}}">
                            </div>
                            <div class="form-group">
                                <label for="position">Job Position</label>
                                <input type="text" class="form-control" id="position" rows="1" placeholder="Graduate assistant" name="position" value="{{$student->position}}">
                            </div>
                            <div class="form-group">
                                <label for="enrolled">Enrolled</label>
                                <input type="text" class="form-control" id="enrolled" placeholder="Yes" name="enrolled" value="{{$student->enrolled}}">
                            </div>
                            <div class="form-group">
                                <button class="btn btn-primary" type="submit">Edit Student</button>
                            </div>
                        </form>
                        <form action="destroy" method="post">
                            @method('PUT')
                            @csrf()
                            <input type="hidden" class="form-control disabled" id="id" name="id" value="{{$student->id}}">
                            <div class="form-group">
                                <button class="btn btn-secondary" type="submit">Delete Student</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection