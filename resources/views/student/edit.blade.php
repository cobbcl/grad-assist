@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div>
                <img src = "/images/etsulogo2.png" height ="200" width=730">
            </div>
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">Add a student</div>

                    <div class="card-body">
                        <form action="save" method="post">
                            @csrf()
                            <div class="form-group">
                                <label for="student_id">E Number</label>
                                <textarea class="form-control" id="student_id" rows="1" placeholder="E00000000" name="student_id"></textarea>
                            </div>
                            <div class="form-group">
                                <label for="name">Name</label>
                                <input type="text" class="form-control" id="name" placeholder="Full name" name="name">
                            </div>
                            <div class="form-group">
                                <label for="rating">Rating</label>
                                <input type="text" class="form-control" id="rating" placeholder="5" name="rating">
                            </div>
                            <div class="form-group">
                                <label for="position">Job Position</label>
                                <textarea class="form-control" id="position" rows="1" placeholder="Graduate assistant" name="position"></textarea>
                            </div>
                            <div class="form-group">
                                <label for="enrolled">Enrolled</label>
                                <input type="text" class="form-control" id="enrolled" placeholder="Yes" name="enrolled">
                            </div>
                            <div class="form-group">
                                <div class="form-check">
                                    <input class="form-check-input" type="checkbox" value="" id="invalidCheck" required>
                                    <label class="form-check-label" for="invalidCheck">
                                        This is a valid student.
                                    </label>
                                    <div class="invalid-feedback">
                                        You must agree before submitting.
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <button class="btn btn-primary" type="submit">Add Student</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection