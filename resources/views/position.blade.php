@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div>
                <img src = "/images/etsulogo2.png" height ="200" width="730">
            </div>
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">Add a position</div>
                    <div class="card-body">
                        <form action="update" method="post">
                            @method('PUT')
                            @csrf()
                            <div class="form-group">
                                <label for="position">Position</label>
                                <input type="text" class="form-control disabled" id="position" rows="1" placeholder="Position" name="position" value="{{$position->position}}">
                            </div>
                            <div class="form-group">
                                <label for="pay">Name</label>
                                <input type="text" class="form-control" id="pay" placeholder="10" name="pay" value="{{$position->pay}}">
                            </div>
                            <div class="form-group">
                                <button class="btn btn-primary" type="submit">Add Position</button>
                            </div>
                        </form>
                        <form action="/destroy" method="post">
                            @method('delete')
                            @csrf()
                            <div class="form-group">
                                <button class="btn btn-secondary" type="submit">Delete Position</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection