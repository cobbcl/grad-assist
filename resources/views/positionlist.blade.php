@extends('layouts.app')

@section('content')
    <div class="Divspace"></div>
    <div class="container">
        <div class="row justify-content-center">
            <div>
                <img src = "/images/etsulogo2.png" height ="200" width="730">
            </div>
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">Home Dashboard</div>

                    <div class="card-body">
                        @if (session('status'))
                            <div class="alert alert-success" role="alert">
                                {{ session('status') }}
                            </div>
                        @endif

                        <div class="well">
                            <div class="container">
                                <div class="row">
                                    <div class="col-5">
                                        <ul>
                                            <h5>Positions</h5>
                                            @foreach($students as $student)
                                                <li><a href="/positions/show?positionName={{$position->position_name}}">{{$position['position_name']}}, ${{$position['pay']}} per hour</a></li>
                                            @endforeach
                                        </ul>
                                    </div>
                                    <div class="col-6">
                                        {{--Transfer this via POST data--}}
                                        {{--Do a firstOrCreate--}}
                                        {{--Return the newly created or already existing user--}}
                                        <form method="GET" action="/students/show">
                                            <div class="form-group">
                                                <label for="studentId">Student Searcher</label>
                                                <input type="text" class="form-control" id="studentId" placeholder="E Number" name="studentId">
                                            </div>
                                            <button type="submit" class="btn btn-primary">Search</button>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>