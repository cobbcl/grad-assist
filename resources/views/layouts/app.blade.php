<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <style>

        header, nav, footer {
            background-color: rgb(255,189,13);
            color: #eee;
            height: 20px;
            width:  100%;
            margin: 0;
            padding: 10px 10px;
            text-align: center;
        }
        nav {
            padding-bottom: 20px;
            background-color: #062766;
            height: 30px;
            overflow: hidden;
        }
        .top-left {
            position: absolute;
            left: 20px;
            top: 40px;
            color: rgb(254,254,254);
            font-size: 20px;
            font-weight: bold;
        }
        li {
            float: left;
            color: rgb(254,254,254);
        }
        .loginButton{
            width: 300px;
            height: 40px;
            background-color: #062766;
            color: rgb(255,255,255);
            font-size: 20px;
            font-weight: bold;
            float: left;
        }
        .registerButton{
            width: 300px;
            height: 40px;
            background-color: #062766;
            color: rgb(255,255,255);
            font-size: 20px;
            font-weight: bold;
        }home

    </style>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Etsu | GradAssist') }}</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet" type="text/css">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
</head>
<body background="/images/background.png">
<header>

</header>
<nav></nav>
    <div id="app">
        <nav class="navbar navbar-expand-md navbar-light navbar-laravel">

            <div class="loginButton">ETSU Grad Assistance</div>
            <div class="container">
                <a class="loginButton" href="{{ url('/home') }}">
                    Home
                </a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <!-- Left Side Of Navbar -->
                    <ul class="navbar-nav mr-auto">

                    </ul>

                    <!-- Right Side Of Navbar -->
                    <ul class="navbar-nav ml-auto">
                        <!-- Authentication Links -->
                        @guest
                            <li>
                                <a href="{{ route('login') }}" class="loginButton">{{ __('Login') }}</a>
                            </li>
                            <li >
                                @if (Route::has('register'))
                                    <a  href="{{ route('register') }}" class="registerButton">Register</a>
                                @endif
                            </li>
                        @else
                            <li class="nav-item dropdown">
                                <a id="navbarDropdown" class="loginButton" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>User:
                                    {{ Auth::user()->name }} <span class="caret"></span>
                                </a>

                                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                    <a class="dropdown-item" href="{{ route('home') }}">
                                        {{ __('Home') }}
                                    </a>

                                    

                                    <a class="dropdown-item" href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                        {{ __('Logout') }}
                                    </a>

                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        @csrf
                                    </form>
                                </div>
                            </li>
                        @endguest
                        <li>
                            <a class ="loginButton" href="{{ route('logout') }}"}
                               onclick="event.preventDefault();
                            document.getElementById('logout-form').submit();">Logout
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>

        <main class="py-4">
            @yield('content')
        </main>
    </div>
</body>
</html>
