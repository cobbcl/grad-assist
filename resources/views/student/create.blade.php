@extends('layouts.app')

@section('content')
    <div class="Divspace"></div>
    <div class="Divspace"></div>

    <div class="container">
        <div class="row justify-content-center">
            <div>
                <img src = "/images/etsulogo2.png" height ="200" width="600">
            </div>
            <div class="createHeader">Create a new student?</div>
            <div class="createBox">

                <ul>
                    <li><a href="/home" class="NoButton">No</a></li>

                    <li><a href="/students/edit" class="YesButton">Yes</a></li>

                </ul>
            </div>
        </div>
    </div>


@endsection

