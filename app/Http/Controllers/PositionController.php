<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class PositionController extends Controller
{
    /**
     * Display the specified resource.
     *
     * @param $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request)
    {
        $positionName = $request->input('position');

        $student = Student::where('student_id', $studentId)->first();

        if($student == null) {
            return view('student.create');
        }

        return view('student.show', compact('student'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input = $request->input();

        $student = new Student;

        $student->name = $input['name'];
        $student->student_id = $input['student_id'];
        $student->rating = $input['rating'];
        $student->enrolled = $input['enrolled'];
        $student->position = $input['position'];

        $student->save();

        return redirect('home', 201);
    }
}
