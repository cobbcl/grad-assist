<?php

namespace App\Http\Controllers;

use App\Student;
use Illuminate\Http\Request;

class StudentsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input = $request->input();

        $student = new Student;

        $student->name = $input['name'];
        $student->student_id = $input['student_id'];
        $student->rating = $input['rating'];
        $student->enrolled = $input['enrolled'];
        $student->position = $input['position'];

        $student->save();

        return redirect('home', 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request)
    {
        $studentId = $request->input('studentId');

        $student = Student::where('student_id', $studentId)->first();

        if($student == null) {
            return view('student.create');
        }

        return view('student.show', compact('student'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id = -1)
    {
        return view('student.edit')->with($id);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $input = $request->input();

        $student =  Student::where('student_id', $input['student_id'])->first();

        $student->name = $input['name'];
        $student->student_id = $input['student_id'];
        $student->rating = $input['rating'];
        $student->enrolled = $input['enrolled'];
        $student->position = $input['position'];

        $student->save();

        return redirect('home', 201);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $input = $request->input();

        $student = Student::where('id', $input['id'])->first();
        $student->delete();

        return redirect('home');
    }
}
